<?php
include '../vendor/autoload.php';

use ApexCli\Api;
use ApexCli\ApiConfig;
use ApexCli\Model\Entity;

$cf = [
    'tenant' => 'hanfrucksack',
    'baseUrl' => 'https://api.accx.ch',
    'authKey' => '88ae4e8eb7c118d8648111c0555f736e'
];

$cf = new ApiConfig($cf);
$client = new Api($cf);

$res = $client->data([Entity::CONTACTS]);
print_r($res);