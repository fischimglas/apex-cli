## Fetch Data

Available data sources:

`
contacts, orders, crafts, stories
`

#### Basics
```php
$client = new Api($cf);
$res = $client->data(['contacts']);
```