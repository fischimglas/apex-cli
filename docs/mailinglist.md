### Mailinglist

#### Subscribe
Will only add to Contacts if not already registered. The E-Mail Address receives an confirmation E-Mai. The subscription
must be confirmed with the code sent to that E-Mail Address
````php 
$client->mailinglistSubscribe('jam@paropakaram.com', 'de');
````


#### Confirm Subscription

If the code matches, the subscription is confirmed.
````php 
$client->mailinglistConfirm($code);
````

#### Unsubscribe

This Endpoint is subject to change! The corresponding E-Mail Address will be marked as unsubscribed.
````php 
$client->mailinglistUnsubscribe($contactCode);

````
