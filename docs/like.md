### Url Marks

Marks for URLS. Can be used without limitations. Restrict the user by your session_id, if required.

Mark an URL as read.
````php 
$client->urlMarkRead('/product/1');
````


Mark an URL as liked.
````php 
$client->urlMarkLiked('/product/1');
````


Get all marks for this URL
````php 
$client->urlMarks('/product/1');
````

