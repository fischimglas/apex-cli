# APEX CLI

## Config and Init

For public calls, no authKey is required.

````php 
$cf = [
'tenant' => 'hanfrucksack',
'baseUrl' => 'https://api.paropakaram.com',
'authKey' => API-KEY
];

$cf = new \Apex\ApiConfig($cf);
$client = new \Apex\Api($cf);
````

The client only connects to the server on request, not on initialization.

## [- Cart, Order and Payment](./order.md)
## [- Voucher codes](./voucher.md)
## [- Mailinglist](./mailinglist.md)
## [- Like and Page Marks](./like.md)


## Public data

Fetches only public data. Does not return offline records.

````php 
$array = $client->fetch([Entity::CRAFTS, Entity::MATERIALS]);
$array = $client->fetch([Entity::ALL]);
````


### Submit Form

##### Types

**contact**
Will try to detect the contact and store the update to the contact

**customform**
Will simply safe the form and add a history entry

Uses the Model to verify format validity

````php 

$form = new \Apex\Model\Form();
$form->read([
    'formData' => [
        'firstname' => 'Jamson ' . date('H:i:s'),
        'email' => 'jam@paropakaram.com',
        'message' => 'Hello some Message',
    ],
    'cf' => [
        'type' => 'contact'
    ]
]);
$client->submitForm($Form);
````



## RAW API Calls

You can call any endpoint on the server with a manual configuration, but you should not use this directly. Instead, use
the prepared functions for specific calls, which validate in and output and handle errors. Raw calls can result in
system damage.

````php 
$json = $client->call($method, $url, $data);
````
