## Order

### Order Model

To communicate with the Server, your order data must be wrapped in an Order Model. This way we assure data consistency.

````php 
$orderData = [
    'cart' => [
        [
            'slug' => 'abenteuer',
            'amount' => 2,
            'variationId' => 300,
        ]
    ],
    'contact' => [
        'country' => 'CH'
    ],
    'meta' => [
        'paymentMethod' => 'bank',
        'language' => 'de',
        'currency' => 'CHF'
    ]
];

$o = new \Apex\Order();
$o->read($orderData);
$o->isValid();
````

@See [sampleOrder.json](sampleOrder.json) for Datastructures

### Order API

#### Calculate Shipment Cost

The minimal required information to calculate the shipment cost must be sent in the following structure. The API returns
the parsed and filled order

````php 
$o = new \Apex\Order();
$o->read([
    'cart' => [
        [
            'slug' => 'abenteuer',
            'amount' => 2,
            'variationId' => 300,
        ]
    ],
    'contact' => [
        'country' => 'CH'
    ],
    'meta' => [
        'paymentMethod' => 'bank',
        'language' => 'de',
        'currency' => 'CHF'
    ]
]);
$res = $client->calcShipmentCost($o);
````

#### Update Cart

````php 
$client->orderUpdateCart($o);
````

Saves the order as "draft". The contact is saved for this order, but the contact data is not stored in your contacts.
The API returns

##### Response

````json
{
  "orderId": "498ea1cb31c6a3a0ec2759c1ffe040d1",
  "shippingCost": 7,
  "weight": 2000,
  "deliveryDays": 4
}
````

#### Order Payment Ticket for Stripe

````php 
$client->orderUpdateCart($o);
````

##### Response

````json
{
  "clientSecret": "pi_1IzTjiEbUelqMekqo97tZfjl_secret_LoB3sDkriXD1xESOLNGlGhpPh"
  "paymentIntent": {
    "country": "CH",
    "receipt_email": "jvo.maurer@gmail.com",
    "amount": 49,
    "currency": "CHF"
  }
}
````

#### Confirm Order Unpaid

````php 
$client->orderConfirmUnpaid($o);
````

Seals the order and confirms it by Email. Contact is updated in Contacts.

#### Confirm Order Paid

````php 
$client->orderConfirmPaid($o, $paymentReceipt);
````

Seals the order and confirms it by Email and marks it as Paid. Contact is updated in Contacts.

#### Customer Oder Service Feedback

Save a feedback for a customer.

````php 

$feedbackData = [
    'message' => 'Some message',
    'rating' => 5
];

$client->orderServiceFeedback($orderId, $feedbackData);
````