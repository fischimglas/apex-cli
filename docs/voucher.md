### Redeem Voucher

Voucher Code restricted to an email address require an orderId. If the orderId is not given, the discount amount can not
be calculated!

````php 
$client->voucherRedeem($voucherCode, $orderId);
````

#### Response
````json
{
  "code": "JAM3",
  "info": "Lalala",
  "benefits": "30%; free shipping",
  "discount": 21,
  "freeShipping": true
}
````
