<?php
/**
 *
 */
declare(strict_types=1);

namespace ApexCli;

/**
 *
 * @deprecated
 */
class ApexCache
{
    public array $pages = [];
    public static ?ApexCache $instance = null;

    private function __construct()
    {
        $this->pages = [];
    }

    public static function init(): ApexCache
    {
        if (!self::$instance) {
            self::$instance = new ApexCache();
        }
        return self::$instance;
    }

    public function write(string $name, ?array $data = null): ?int
    {
        unset($this->pages[md5($name)]);

        return file_put_contents($this->filePath($name), json_encode(is_array($data) ? $data : []));
    }

    public function read(string $name): ?array
    {
        $hash = md5($name);
        $cache = $this->pages[$hash] ?? null;

        if (null === $cache) {
            $file = $this->filePath($name);
            if (!file_exists($file)) {
                return null;
            }

            $content = file_get_contents($file);
            $this->pages[$hash] = is_string($content) ? json_decode($content, true) : [];
        }

        return $this->pages[$hash] ?? null;
    }

    public function has(string $name): bool
    {
        return file_exists($this->filePath($name));
    }

    public function filePath(string $name): string
    {
        $path = $_SERVER['DOCUMENT_ROOT'].'/../apex';

        return $path.'/cache-'.$name.'.json';
    }

    public function record($name, $identifier, $fieldName = 'slug'): ?array
    {
        $records = $this->read($name);
        $i = array_search($identifier, array_column((array)$records, $fieldName), true);
        if ($i > -1) {
            return $records[$i];
        }
        return null;
    }

    public function updateData(Api $cli): void
    {
        ob_end_flush();
        $tmp_json = $cli->fetchJson('/tenant/current');
        $this->write('cf', $tmp_json['cf']);
        $this->write('tenant', $tmp_json['tenant']);

        $tmp_json = $cli->call('POST', '/wishlist/suggestions');
        $this->write('suggestions', json_decode($tmp_json));

        $tmp_json = $cli->fetchJson('/craft');
        $this->write('product', $tmp_json['records']);
        $this->write('category', $tmp_json['categories']);

        $data = ['partial', 'slide', 'page', 'story', 'crew', 'organisation', 'referral', 'artisan', 'text', 'material', 'social'];
        foreach ($data as $key) {
            $this->fetchAndCache($key, $cli);
        }

        $this->cacheForms();

        $this->write('update', [
            'date' => date('Y-M-D H:i:s')
        ]);
    }

    public function cacheForms()
    {
        $forms = [];

        $data = ['partial', 'slide', 'story', 'crew', 'organisation', 'referral', 'artisan', 'material', 'social'];
        foreach ($data as $key) {
            $raw = $this->read($key);
            foreach ($raw as $item) {
                foreach ($item['translations'] as $lang => $tr) {
                    $this->readForms($tr['content'], $forms);
                }
            }
        }

        $data = ['page'];
        foreach ($data as $key) {
            $raw = $this->read($key);
            foreach ($raw as $item) {
                foreach ($item['content'] as $lang => $tr) {
                    foreach ($tr as $pageElement) {
                        if ($pageElement['type'] === 'content') {
                            $this->readForms($pageElement['value'], $forms);
                        }
                    }
                }
            }
        }
        $this->write('forms', $forms);
    }

    public function fetchAndCache($name, Api $cli): ?int
    {
        return $this->write($name, $cli->fetchJson('/'.$name));
    }

    public function readForms($content, array &$forms)
    {
        foreach ($content as $contentElement) {
            if ($contentElement['type'] === 'form') {
                $forms[$contentElement['name']] = $contentElement;
            }
        }
    }
}
