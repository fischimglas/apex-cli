<?php
declare(strict_types=1);

namespace ApexCli\Data;

use ApexCli\ApexCache;
use ApexCli\ApexHelper;
use ApexCli\Filter\ProductFlattenFilter;
use Sulaco\Date\DateFormat;

class ApexData
{
    public static function cache(): ApexCache
    {
        return ApexCache::init();
    }

    public static function form(string $slug): ?array
    {
        $records = self::cache()->read('forms');

        return $records[$slug] ?? null;
    }

    public static function page(string $slug): ?array
    {
        return self::cache()->record('page', $slug);
    }

    public static function pageById(int $recordId): ?array
    {
        return self::cache()->record('page', $recordId, 'id');
    }

    public static function material(string $slug): ?array
    {
        return self::cache()->record('material', $slug);
    }

    public static function materials(): ?array
    {
        return self::cache()->read('material');
    }

    public static function social(string $slug): ?array
    {
        return self::cache()->record('social', $slug);
    }

    public static function socials(): ?array
    {
        return array_filter(self::cache()->read('social'), fn($it) => $it['status'] === 'online');
    }

    public static function pages(): ?array
    {
        return array_filter(self::cache()->read('page'), fn($it) => ($it['status'] ?? null) === 'online');
    }

    public static function texts(): ?array
    {
        return self::cache()->read('text');
    }

    public static function text(string $slug): ?array
    {
        return self::cache()->record('text', $slug);
    }

    public static function t(string $slug): string
    {
        $res = '';
        $rec = self::text($slug);
        if ($rec) {
            $res = $rec['tr']['de'];
        }

        return $res;
    }

    public static function pagesInNav(): ?array
    {
        return array_filter(self::pages(), fn($it) => ($it['showInNav'] ?? false) === true);
    }

    public static function organisations(): ?array
    {
        return array_filter(self::cache()->read('organisation'), fn($it) => $it['status'] === 'online');
    }

    public static function partners(): ?array
    {
        return array_filter(self::organisations(), fn($it) => in_array('partner', $it['category'], true));
    }

    public static function producers(): ?array
    {
        return array_filter(self::organisations(), fn($it) => in_array('producers', $it['category'], true));
    }

    public static function producer(?string $slug): ?array
    {
        return self::cache()->record('producer', $slug);
    }

    public static function pos(): ?array
    {
        return array_filter(self::organisations(), fn($it) => in_array('pos', $it['category'], true));
    }

    public static function crews(): ?array
    {
        return array_filter(self::cache()->read('crew'), fn($it) => $it['status'] === 'online');
    }

    public static function referrals(): ?array
    {
        return array_filter(self::cache()->read('referral'), fn($it) => $it['isOnline'] === true);
    }

    public static function artisans(): ?array
    {
        return array_filter(self::cache()->read('artisan'), fn($it) => $it['status'] === 'online');
    }

    public static function artisan(int $id): ?array
    {
        return self::cache()->record('artisan', $id, 'id');
    }

    public static function crewMember(int $id): ?array
    {
        return self::cache()->record('crew', $id, 'id');
    }

    public static function organisation($id, ?string $fieldName = 'slug'): ?array
    {
        return self::cache()->record('organisation', $id, $fieldName);
    }

    public static function story($slug, $fieldName = 'slug'): ?array
    {
        return self::cache()->record('story', $slug, $fieldName);
    }

    public static function storiesByIds(array $ids)
    {
        return array_map(fn($id) => self::story($id, 'id'), $ids);
    }

    public static function partial(string $slug): ?array
    {
        return self::cache()->record('partial', $slug);
    }

    public static function products(): ?array
    {
        return ProductFlattenFilter::filter(self::cache()->read('product'));
    }

    public static function suggestions(): ?array
    {
        return self::cache()->read('suggestions');
    }

    public static function product($slug, string $idField = 'slug'): ?array
    {
        $products = self::products();
        foreach ($products as $product) {
            if ((string)$product[$idField] === (string)$slug) {
                return $product;
            }
        }

        return null;
    }

    public static function productVariation(string $slug, string $variationSlug): ?array
    {
        $product = null;
        $products = self::products();
        $slug = trim($slug);
        $variationSlug = trim($variationSlug);

        $variationSlugs = array_column($products, 'variationSlug');
        $slugs = array_column($products, 'slug');

        $candidates = array_filter($products, fn($it) => $variationSlug === $it['variationSlug']);
        $candidates = array_filter($candidates, fn($it) => $slug === $it['slug']);

        $result = array_shift($candidates);

        return $result;
    }

    public static function productSiblings(string $slug): array
    {
        return array_filter(self::products(), fn($it) => $it['slug'] === $slug);
    }

    public static function slides(): ?array
    {
        return array_filter(self::cache()->read('slide'), fn($it) => $it['status'] === 'online');
    }

    public static function categories(): ?array
    {
        $categories = array_filter(self::cache()->read('category'), fn($it) => $it['status'] === true);

        uasort($categories, fn($a, $b) => ($a['si'] < $b['si']) ? -1 : 1);

        return $categories;
    }

    public static function stories(): ?array
    {
        return array_filter(self::cache()->read('story'), fn($it) => $it['status'] === 'online');
    }

    public static function category(?string $name = null): ?array
    {
        if (!$name) {
            return [];
        }
        $cats = self::categories();
        foreach ($cats as $cat) {
            if ($cat['slug'] === $name) {
                return $cat;
            }
        }

        foreach ($cats as $cat) {
            foreach ($cat['subcategories'] as $sub) {
                if ($sub['slug'] === $name) {
                    return $sub;
                }
            }
        }

        return null;
    }

    public static function parentCategory($name): ?array
    {
        $categories = self::categories();
        foreach ($categories as $cat) {
            foreach ($cat['subcategories'] as $sub) {
                if ($sub['slug'] === $name) {
                    return $cat;
                }
            }
        }

        return null;
    }

    public static function productsByFlag($flag): array
    {
        $res = [];
        foreach (self::products() as $p) {
            if (is_array($p['flags']) && array_search($flag, $p['flags'], true) > -1) {
                $res[] = $p;
            }
        }

        return $res;
    }

    public static function productsByCategory(string $slug): array
    {
        return array_filter(self::products(), fn($it) => $it['category'] === $slug);
    }

    public static function productsByCart(array $cart): array
    {
        return array_map(fn($cartItem) => self::product($cartItem['variationId'], 'variationId'), $cart);
    }

    public static function cartWithProducts(array $cart): array
    {
        return array_map(fn($cartItem) => array_merge($cartItem, ['product' => self::product($cartItem['variationId'], 'variationId')]), $cart);
    }

    public static function productsByTag(string $tag, $lang = 'en'): array
    {
        $res = [];
        foreach (self::products() as $p) {
            if (count($p['tags'][$lang])) {
                foreach ($p['tags'][$lang] as $tt) {
                    if ($tt === $tag) {
                        $res[] = $p;
                    }
                }
            }
        }

        return $res;
    }

    public static function siteUrls(): array
    {
        $nav = [];
        $blacklist = ['checkout', 'test', 'shopSuccess', 'mailinglistConfirmed', 'success', 'maintenance', 'membershipActive', 'shopFeedback', 'shopCheckout', 'error', 'wishlist'];
        $pages = self::pages();
        foreach ($pages as $item) {
            $slug = $item['slug'];
            if (in_array($slug, $blacklist)) {
                continue;
            }
            $url = trim($item['url'] ?? '');
            $url = strlen($url) > 2 ? $url : null;

            $navItem = [
                'lastmod' => date(DateFormat::FORMAT_MYSQL_DATE, strtotime($item['dateUpdated'] ?? date(DateFormat::FORMAT_MYSQL_DATE))),
                'loc' => $url ?: '/pages/'.$item['slug'],
            ];
            $nav[] = $navItem;
        }

        $records = self::products();
        foreach ($records as $product) {
            $nav[] = [
                'loc' => ApexHelper::productUrl($product),
                'lastmod' => date(DateFormat::FORMAT_MYSQL_DATE, strtotime($record['dateUpdated'] ?? date(DateFormat::FORMAT_MYSQL_DATE)))
            ];
        }

        $records = self::categories();
        foreach ($records as $record) {
            $nav[] = [
                'loc' => ApexHelper::categoryUrl($record),
                'lastmod' => date(DateFormat::FORMAT_MYSQL_DATE, strtotime($record['dateUpdated'] ?? date(DateFormat::FORMAT_MYSQL_DATE)))
            ];
        }

        $records = self::socials();
        foreach ($records as $record) {
            $nav[] = [
                'loc' => ApexHelper::materialUrl($record),
                'lastmod' => date(DateFormat::FORMAT_MYSQL_DATE, strtotime($record['dateUpdated'] ?? date(DateFormat::FORMAT_MYSQL_DATE)))
            ];
        }

        $records = self::materials();
        $records = array_filter($records, fn($it) => $it['isListed'] === true);
        foreach ($records as $record) {
            $nav[] = [
                'loc' => ApexHelper::materialUrl($record),
                'lastmod' => date(DateFormat::FORMAT_MYSQL_DATE, strtotime($record['dateUpdated'] ?? date(DateFormat::FORMAT_MYSQL_DATE)))
            ];
        }
        //$records = self::organisations();
        //$records = array_filter($records, fn($it) => $it['showDetail'] === true);
        //foreach ($records as $record) {
        //    $nav[] = [
        //        'loc' => '/organisation/'.$record['slug'],
        //        'lastmod' => date(DateFormat::FORMAT_MYSQL_DATE, strtotime($record['dateUpdated'] ?? date(DateFormat::FORMAT_MYSQL_DATE)))
        //    ];
        //}

        $records = self::stories();
        foreach ($records as $record) {
            $nav[] = [
                'loc' => ApexHelper::blogUrl($record),
                'lastmod' => date(DateFormat::FORMAT_MYSQL_DATE, strtotime($record['dateUpdated'] ?? date(DateFormat::FORMAT_MYSQL_DATE)))
            ];
        }

        return $nav;
    }
}