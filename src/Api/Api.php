<?php
declare(strict_types=1);

namespace ApexCli;

use ApexCli\Model\Form;
use ApexCli\Model\Order;
use ApexCli\Model\PaymentIntent;
use Exception;
use GuzzleHttp\Client;

class Api
{
    private Client $client;
    private ApiConfig $cf;

    public function __construct(?ApiConfig $cf = null)
    {
        $cf = $cf ?: new ApiConfig();
        $this->cf = $cf;
        $this->client = new Client([
            'base_uri' => $cf->baseUrl,
            'timeout' => 20.0,
        ]);
    }

    public function call($method, $url, ?array $json = null): string
    {
        $method = strtoupper($method);
        $params = [];
        if ($method === 'POST' || $method === 'PUT') {
            $params['json'] = $json;
        }
        $params['headers'] = [
            'X-Apex-Tenant' => $this->cf->tenant,
            'X-Auth' => $this->cf->authKey
        ];
        $res = $this->client->request($method, $url, $params);
        return $res->getBody() ? $res->getBody()->getContents() : '[]';
    }

    public function fetchJson(string $url): ?array
    {
        $data = $this->call('GET', $url);
        if (!$data) {
            return null;
        }
        try {
            return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            // jam($data, $e);
            echo 'JSON Syntax Error '.$url;
            print_r($data);
        }
        return null;
    }

    public function data(array $entities): array
    {
        return json_decode($this->call('GET', '/load/'.implode(',', $entities)), true, 512);
    }

    public function orderUpdateCart(Order $order): ?array
    {
        $data = (array)$order;
        if (isset($data['paymentMethod'])) {
            $data['meta']['paymentMethod'] = $data['paymentMethod'];
            unset($data['paymentMethod']);
        }

        $res = $this->call('POST', '/shop/updateCart', $data);
        $response = json_decode($res, true);
        if (!$response) {
            return ['error' => $res];
        }
        $order->orderId = $response['orderId'];
        return $response;
    }


    public function calcShipmentCost(Order $order)
    {
        $res = $this->call('POST', '/shop/shipmentCost', (array)$order);
        return json_decode($res, true);
    }

    public function orderConfirmUnpaid(Order $o)
    {
        $res = $this->call('POST', '/shop/confirm/unpaid/'.$o->orderId);
        return json_decode($res, true);
    }

    public function orderConfirmPaid($orderId, array $paymentReceipt)
    {
        $res = $this->call('POST', '/shop/confirm/paid/'.$orderId, $paymentReceipt);
        return json_decode($res, true);
    }

    public function orderById(string $orderId)
    {
        $res = $this->call('GET', '/order/'.$orderId.'/byOrderId');
        return json_decode($res, true);
    }

    public function orderMarkReloaded(string $orderId)
    {
        $res = $this->call('GET', '/order/'.$orderId.'/markReloaded');
        return json_decode($res, true);
    }

    public function paymentTicket(PaymentIntent $intent)
    {
        $d = (array)$intent;
        $res = $this->call('POST', '/shop/pos-ticket', ['intentData' => $d]);
        return json_decode($res, true);
    }

    public function orderPaymentTicket(string $orderId)
    {
        $res = $this->call('POST', '/order/'.$orderId.'/pos-ticket');
        return json_decode($res, true);
    }

    public function orderServiceFeedback(array $data)
    {
        $res = $this->call('POST', '/order/servicefeedback', $data);
        return json_decode($res, true);
    }

    public function fetch(array $dataSources): array
    {
        return json_decode($this->call('GET', '/load/'.implode(',', $dataSources)), true, 512, JSON_THROW_ON_ERROR);
    }

    public function mailinglistSubscribe(string $emailAddress, ?string $languageCode = null)
    {
        $data = ['email' => $emailAddress, 'language' => $languageCode ? strtolower($languageCode) : null];
        return $this->call('POST', '/mailinglist/subscribe', $data);
    }

    public function membershipSignup(array $data, ?string $languageCode = null)
    {
        $data['language'] = $languageCode;
        return $this->call('POST', '/membership/subscribe', $data);
    }

    public function membershipActivate(string $code)
    {
        return $this->call('POST', '/membership/activate/'.$code);
    }

    public function mailinglistConfirm(string $code)
    {
        return $this->call('POST', '/mailinglist/confirm', ['code' => $code]);
    }

    public function mailinglistUnsubscribe(string $contactCode)
    {
        return $this->call('POST', '/mailinglist/confirm', ['code' => $contactCode]);
    }

    public function urlMarkLiked(string $url)
    {
        return $this->call('POST', '/url/like', ['url' => $url]);
    }

    public function urlMarkRead(string $url)
    {
        return $this->call('POST', '/url/read', ['url' => $url]);
    }

    public function urlMarks(string $url)
    {
        return $this->call('POST', '/url/get', ['url' => $url]);
    }

    public function updateWishlist(string $sessionId, array $products)
    {
        return $this->call('POST', '/wishlist', [
            'sessionId' => $sessionId,
            'products' => $products,
        ]);
    }

    public function variationViewCount(int $variationId, ?string $sessionId = null)
    {
        return $this->call('POST', '/variation/'.$variationId.'/count', ['sessionId' => $sessionId]);
    }

    public function submitForm(Form $form)
    {
        return $this->call('POST', '/formsubmit', (array)$form);
    }

    public function voucherRedeem(string $voucherCode, ?string $orderId = null)
    {
        $res = $this->call('POST', '/voucher/redeem', [
            'code' => $voucherCode,
            'orderId' => $orderId
        ]);

        return json_decode($res, true);
    }

    public function voucherLoad(string $voucherCode)
    {
        $res = $this->call('POST', '/voucher/load', [
            'code' => $voucherCode
        ]);

        return json_decode($res, true);
    }

    public function calculateShipmentCost(Order $order)
    {
        $res = $this->call('POST', '/shop/shipmentCost', (array)$order);

        return json_decode($res, true);
    }

    public function reportError(mixed $error = null, ?string $message = null)
    {
        $res = $this->call('POST', '/system/reportError', [
            'tenant' => $this->cf->tenant,
            'error' => $error,
            'message' => $message,
        ]);

        return json_decode($res, true);
    }


    public function countView(string $path, ?string $sessionId = null)
    {
        return $this->call('POST', '/website/cv', ['sessionId' => $sessionId, 'path' => $path]);
    }
}