<?php
declare(strict_types=1);

namespace ApexCli;


class ApiConfig
{
    public string $tenant = 'paropakaram';
    public string $baseUrl = 'https://api.accx.ch';
    public ?string $authKey = null;

    public function __construct(?array $cf = null)
    {
        if (!is_array($cf)) {
            return;
        }
        foreach ($cf as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            }
        }
    }
}