<?php
declare(strict_types=1);

namespace ApexCli;

use ApexCli\Data\ApexCf;
use ApexCli\Data\ApexData;
use ApexCli\Model\Form;

class ApexHelper
{
    public static function localizePrice(?float $price = null): ?float
    {
        if (!$price) {
            return null;
        }
        $cur = ApexCf::currency();
        $base = strtoupper($cur['base'] ?? 'CHF');
        $rates = [];
        foreach ($cur['rates'] as $it) {
            $rates[$it['slug']] = $it['rate'];
        }


        if ($base !== 'CHF') {
            $price = $rates['CHF'] * $price;
            $price = round($price * 20) / 20;
        }

        return $price;
    }

    public static function loadForm(string $name)
    {
        $formCache = ApexData::form($name);
        $form = new Form();
        $form->setCf([
            'name' => $name,
            'actions' => $formCache['actions'] ?? null
        ]);

        return $form;
    }

    public static function discountPercent(int $oldPrice, int $newPrice): float
    {
        if($newPrice <= 0) {
            return 0;
        }
        return 100 - ceil((100 / $oldPrice) * $newPrice);
    }

    public static function discountAmount(int $oldPrice, int $newPrice): float
    {
        return $oldPrice - $newPrice;
    }

    public static function sanitizeUrl(?string $url = null): string
    {
        if ($url === null) {
            return '';
        }

        if (!preg_match('/^(http|https)/', $url, $regs)) {
            $url .= 'http://'.$url;
        }
        return $url;
    }

    public static function urlH(?string $url = null): string
    {
        if ($url === null) {
            return '';
        }

        $url = preg_replace('/^https:\/\//', '', $url);
        $url = preg_replace('/^http:\/\//', '', $url);
        $url = preg_replace('/^www\./', '', $url);
        $url = preg_replace('/\/$/', '', $url);

        return $url;
    }

    public static function materialTotal(array $cartWithproducts): float
    {
        return array_sum(array_map(static function ($cartItem) {
            $quantity = $cartItem['amount'] ?? 1;
            $quantity = (int)$quantity;
            $price = self::productPrice($cartItem['product']) ?? 0;

            return $quantity * $price;
        }, $cartWithproducts));
    }

    public static function productPrice(?array $product = null): ?float
    {
        if (!$product) {
            return null;
        }
        $price = $product['price'] ?? null;
        $priceSale = $product['priceSale'] ?? null;

        return self::localizePrice(self::isSale($product) ? $priceSale : $price);
    }

    public static function isSale(?array $product = null): bool
    {
        return ($product['priceSale'] ?? null) > 0;
    }

    public static function urlPatternReplace(string $url, array $product)
    {
        preg_match_all('/%([a-z]+)%/i', $url, $regs);
        $names = $regs[1];
        $patterns = $regs[0];
        $tr = [];
        foreach ($names as $i => $name) {
            $tr[$patterns[$i]] = $product[$name] ?? '';
        }

        return str_replace(array_keys($tr), array_values($tr), $url);
    }

    public static function productUrl(?array $product = null): ?string
    {
        if (!$product) {
            return null;
        }

        return self::urlPatternReplace(ApexCf::productUrlPattern(), $product);
    }

    public static function categoryUrl(array $cat): ?string
    {
        return self::urlPatternReplace(ApexCf::categoryUrlPattern(), $cat);
    }

    public static function blogUrl(array $record): ?string
    {
        return self::urlPatternReplace(ApexCf::blogUrlPattern(), [
            'slug' => urlencode($record['slug']),
            'date' => date('Y-m-d', strtotime($record['dateCreated'])),
        ]);
    }

    public static function materialUrl(array $record): ?string
    {
        return self::urlPatternReplace(ApexCf::materialUrlPattern(), [
            'slug' => urlencode($record['slug']),
        ]);
    }

    public static function socialProjectUrl(array $record): ?string
    {
        return self::urlPatternReplace(ApexCf::socialProjectUrlPattern(), [
            'slug' => urlencode($record['slug']),
        ]);
    }

    public static function organisationUrl(array $record): ?string
    {
        return self::urlPatternReplace(ApexCf::organisationUrlPattern(), [
            'slug' => urlencode($record['slug']),
        ]);
    }

    public static function cloudinaryImage($publicId, array $baseCf = []): string
    {
        $baseCf = array_merge([
            'gravity' => 'center',
            'crop' => 'fit',
            'quality' => 95,
            'width' => 400,
        ], $baseCf);

        $tr = [
            'gravity' => 'g',
            'crop' => 'c',
            'width' => 'w',
            'height' => 'h',
            'quality' => 'q',
            'fl' => 'fl',
            'format' => 'f',
        ];

        foreach ($baseCf as $key => $value) {
            $cf[] = $tr[$key].'_'.$value;
        }
        $cfString = implode(',', $cf);

        return 'https://res.cloudinary.com/'.ApexCf::cloudName().'/image/upload/'.$cfString.'/v1/'.$publicId;
    }


    public static function metaToClasses(mixed $meta): array
    {
        $classes = [];
        $sides = ['top', 'right', 'bottom', 'left'];
        foreach ($sides as $side) {
            foreach ($meta[$side] as $key => $value) {
                if ($meta[$side][$key] > 0) {
                    $classes[] = $key.substr($side, 0, 1).'-'.$value;
                }
            }
        }

        return $classes;
    }

    public static function T($slug): ?string
    {
        return ApexData::text($slug)['tr'][ApexCf::i18nDefault()] ?? null;
    }
}