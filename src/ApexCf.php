<?php
declare(strict_types=1);

namespace ApexCli\Data;

use Sulaco\Exception\ConfigException;

class ApexCf
{
    private static null|array $apex = null;

    /**
     * Apex local tenant config, from apex.json
     */
    public static function apex(): array
    {
        if (self::$apex === null) {
            $file = $_SERVER['DOCUMENT_ROOT'].'/../apex.json';
            if (!file_exists($file)) {
                throw new ConfigException('apex.json not present');
            }
            self::$apex = json_decode(file_get_contents($file), true);
        }

        return self::$apex;
    }


    public static function tenant(): string
    {
        return self::apex()['tenant'];
    }

    public static function isCfPresent(): bool
    {
        return ApexData::cache()->has('cf');
    }

    public static function cf(?string $name = null): array|null
    {
        $cf = ApexData::cache()->read('cf');
        if (!is_array($cf)) {
            return [];
        }
        return $name ? $cf[$name] : $cf;
    }

    public static function website(): array
    {
        return self::cf('website');
    }

    public static function thirdParty(): array
    {
        return self::cf('thirdParty');
    }

    public static function shipmentCost(): array
    {
        return self::cf('shipmentCost');
    }

    public static function formatting(): array
    {
        return self::cf('formatting');
    }

    public static function i18n(): array
    {
        return self::cf('i18n');
    }

    public static function currency(): array
    {
        return self::cf('currency');
    }

    public static function i18nDefault(): string
    {
        return strtolower(self::i18n()['base'] ?? 'de');
    }

    public static function i18nAvailable(): array
    {
        return self::i18n()['available'];
    }

    public static function currencyBase(): string
    {
        return self::currency()['base'];
    }

    public static function currencyAvailable(): array
    {
        return self::currency()['available'];
    }

    public static function dateFormat(): array
    {
        return self::formatting()['dateFormat'];
    }

    public static function countriesAvailable(): ?array
    {
        $availableSlugs = self::cf()['countries']['available'];
        $allCountries = json_decode(file_get_contents(__DIR__.'/../etc/countries.json'), true);
        return array_filter($allCountries, fn($it) => in_array($it['slug'], $availableSlugs));
    }

    public static function websiteUrl(): string
    {
        return self::website()['websiteUrl'];
    }

    public static function productUrlPattern(): string
    {
        return '/produkt/%category%/%slug%/%variationSlug%';
    }

    public static function categoryUrlPattern(): string
    {
        return '/shop/kategorie/%slug%';
    }

    public static function subCategoryUrlPattern(): string
    {
        return '/shop/kategorie/%slug%';
    }

    public static function blogUrlPattern(): string
    {
        return '/blog/%date%/%slug%';
    }

    public static function materialUrlPattern(): string
    {
        return '/material/%slug%';
    }

    public static function socialProjectUrlPattern(): string
    {
        return '/social-project/%slug%';
    }

    public static function organisationUrlPattern(): string
    {
        return '/organisation/%slug%';
    }

    public static function cloudName(): string
    {
        return self::thirdParty()['cloudinary_cloud_name'];
    }

    public static function stripeApiPublicKey(): string
    {
        return self::thirdParty()['stripe_api_public_key'];
    }

    public static function googleAnalyticsKey(): string
    {
        return self::thirdParty()['google_analytics_key'];
    }

    public static function categoriesInNav(): bool
    {
        return self::website()['categoriesInNav'] === true;
    }

    public static function subcategoriesAsDropdown(): bool
    {
        return self::website()['subcategoriesAsDropdown'] === true;
    }

    public static function shopEnabled(): bool
    {
        return self::website()['shopEnabled'] === true;
    }

    public static function websiteEnabled(): bool
    {
        return self::website()['websiteEnabled'] === true;
    }

    public static function bankEnabled(): bool
    {
        return self::website()['bankEnabled'] === true;
    }

    public static function ccEnabled(): bool
    {
        return self::website()['ccEnabled'] === true;
    }

    public static function wishlistEnabled(): bool
    {
        return self::website()['wishlistEnabled'] === true;
    }

    public static function showQuantitySelector(): bool
    {
        return self::website()['showQuantitySelector'] === true;
    }

    public static function showCrossSale(): bool
    {
        return self::website()['showCrossSale'] === true;
    }

    public static function blogEnabled(): bool
    {
        return self::website()['blogEnabled'] === true;
    }

}