<?php

declare(strict_types=1);

namespace ApexCli\Meta;

class Meta
{
    public array $data = [];
    public array $tags = [];

    public function set(string $name, $value)
    {
        $this->data[$name] = $value;
    }

    public function addTags(?array $tags = null)
    {
        if ($tags) {
            $this->tags = array_unique(array_merge($this->tags, $tags));
        }
    }

    public function setImage(string $imagePath, int $w = 500, int $h = 500)
    {
        $this->set('image', $imagePath);
        $this->set('image:width', $w);
        $this->set('image:height', $h);
    }

    public function hasImage(): bool
    {
        return isset($this->data['image']);
    }

    public function get()
    {
        $res = [];
        foreach ($this->data as $key => $value) {
            $res[] = ['property' => $key, 'content' => $value];
        }
        return $res;
    }
}