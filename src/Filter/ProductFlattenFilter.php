<?php
declare(strict_types=1);

namespace ApexCli\Filter;

class ProductFlattenFilter
{
    public static function filter(array $records): array
    {
        $result = [];
        foreach ($records as $it) {
            if (count($it['vars']) > 0) {
                foreach ($it['vars'] as $its) {
                    $newIt = $it;

                    $templateStatus = $it['status'] ?? 'offline';
                    $productStatus = $its['status'] ?? 'offline';
                    $newIt['status'] = ($templateStatus === 'online' && $productStatus === 'online') ? 'online' : 'offline';

                    foreach (['w', 'h', 'l'] as $s) {
                        $tmp = $its['attributes']['dimension'][$s] ?? null;

                        if ($tmp) {
                            $newIt['attributes']['dimension'][$s] = $its['attributes']['dimension'][$s];
                        }
                    }
                    foreach (['weight', 'volume'] as $s) {
                        if (isset($its['attributes'][$s])) {
                            $newIt['attributes'][$s] = $its['attributes'][$s];
                        }
                    }

                    unset($newIt['vars']);


                    $newIt['variationCf'] = $its['cf'];
                    $newIt['isVariation'] = true;
                    $newIt['variationId'] = $its['id'];
                    $newIt['variationSlug'] = $its['slug'];
                    $newIt['variationTr'] = $its['tr'];

                    $newIt['isSoldOut'] = ($its['isSoldOut'] ?? false) === true;
                    $newIt['isUnique'] = ($its['isUnique'] ?? false) === true;

                    $newIt['price'] = $its['price'] ?: $it['price'];
                    $newIt['priceSale'] = $its['priceSale'] ?: $it['priceSale'];
                    $newIt['images'] = $its['images'];

                    $newIt['category'] = $it['category'];
                    $newIt['options'] = $its['options'] ?? [];
                    $newIt['flags'] = array_filter(array_unique(array_merge((array)$it['flags'], (array)$its['flags'])));
                    $result[] = $newIt;
                }
            } else {
                $it['isVariation'] = false;
                $result[] = $it;
            }
        }

        $result = array_filter($result, fn($it) => $it['status'] === 'online');

        return $result;
    }
}