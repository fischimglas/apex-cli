<?php
/**
 *
 */
declare(strict_types=1);

namespace ApexCli;

use Sulaco\Fs\Fs;

class XCache
{
    public array $cache = [];

    public function write(string $name, string $data = null): ?int
    {
        return file_put_contents($this->filePath($name), $data);
    }

    public function read(string $name): ?string
    {
        $hash = md5($name);
        $cache = $this->cache[$hash] ?? null;

        if (null === $cache) {

            $file = $this->filePath($name);
            if (!file_exists($file)) {
                return null;
            }
            $cache = file_get_contents($file);
            $this->cache[$hash] = $cache;
        }

        return $this->cache[$hash] ?? null;
    }

    public function has(string $name): bool
    {
        return file_exists($this->filePath($name));
    }

    public function clear(): void
    {
        $files = Fs::readDirectory($this->path());
        foreach ($files as $file) {
            $name = basename($file);
            if (preg_match('/^html-/', $name)) {
                Fs::deleteFile($file);
            }
        }
    }

    public function path(): string
    {
        return $_SERVER['DOCUMENT_ROOT'].'/../apex';
    }

    public function filePath(string $name): string
    {
        return $this->path().'/html-'.$name.'.html';
    }
}
