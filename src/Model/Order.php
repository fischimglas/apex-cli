<?php

declare(strict_types=1);

namespace ApexCli\Model;


use Exception;

class Order extends AbstractModel
{
    public ?string $orderId = null;
    public array $cart = [];
    public array $contact = [];
    public array $meta = [];

    public function setOrderId(string $orderId): void
    {
        $this->orderId = $orderId;
    }

    public function grandTotal(): int
    {
        $res = 0;
        foreach ($this->cart as $item) {
            $res += $item['amount'] * $item['price'];
        }
        return $res;
    }

    public function isValid(): bool
    {
        $ctFields = ['firstname', 'lastname', 'street', 'zip', 'city', 'country', 'email', 'mobile'];
        foreach ($ctFields as $f) {
            if (!isset($this->contact[$f])) {
                throw new Exception('Contact ' . $f . ' missing');
            }
        }
        $ctFields = ['language', 'currency'];
        foreach ($ctFields as $f) {
            if (!isset($this->meta[$f])) {
                throw new Exception('Meta ' . $f . ' missing');
            }
        }
        $ctFields = ['amount', 'slug', 'price', 'label'];
        foreach ($this->cart as $cartItem) {
            foreach ($ctFields as $f) {
                if (!isset($cartItem[$f])) {
                    return false;
                }
            }
        }
        return true;
    }
}