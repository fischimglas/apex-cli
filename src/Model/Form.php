<?php

declare(strict_types=1);

namespace ApexCli\Model;


class Form extends AbstractModel
{
    public array $cf = [];
    public array $formData = [];

    public function setCf(?array $cf = null)
    {
        $this->cf = $cf;
    }

    public function setData(?array $data = null)
    {
        $this->formData = $data;
    }
}