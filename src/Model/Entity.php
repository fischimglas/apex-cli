<?php

declare(strict_types=1);

namespace ApexCli\Model;


class Entity extends AbstractModel
{
    public const CONTACTS = 'contacts';
    public const CRAFTS = 'crafts';
    public const CATEGORIES = 'categories';
    public const MATERIALS = 'materials';
    public const PAGES = 'pages';
    public const CREWS = 'crews';
    public const ORGANISATIONS = 'organisations';
    public const STORIES = 'stories';

    public const ALL = [
        self::CRAFTS,
        self::CATEGORIES,
        self::MATERIALS,
        self::PAGES,
        self::CREWS,
        self::ORGANISATIONS,
        self::STORIES
    ];
}