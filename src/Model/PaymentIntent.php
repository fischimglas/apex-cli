<?php

declare(strict_types=1);

namespace ApexCli\Model;

class PaymentIntent extends AbstractModel
{
    public string $country = 'ch';
    public string $receipt_email = '';
    public int $amount = 0;
    public string $currency = 'chf';
}