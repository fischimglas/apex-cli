<?php

declare(strict_types=1);

namespace ApexCli\Model;


abstract class AbstractModel
{
    public function read(?array $d = null)
    {
        if ($d) {
            foreach ($d as $k => $v) {
                $this->$k = $v;
            }
        }
    }
}