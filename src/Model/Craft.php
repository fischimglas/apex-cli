<?php

declare(strict_types=1);

namespace ApexCli\Model;

class Craft extends AbstractModel
{
    public string $slug;
}