<?php
/**
 * DEV - unused!
 */

namespace ApexCli;

use ApexCli\Data\ApexData;

class ApexClient
{
    private ?Api $apiClient;

    public function __construct(array $config)
    {
        $cf = new ApiConfig($config);
        $this->apiClient = new Api($cf);
    }

    public function api(): Api
    {
        return $this->apiClient;
    }

    public function data(): ApexData
    {
        return new ApexData();
    }
}